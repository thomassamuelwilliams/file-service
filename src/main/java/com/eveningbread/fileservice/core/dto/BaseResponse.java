package com.eveningbread.fileservice.core.dto;

import com.eveningbread.fileservice.core.constants.ResponseCode;
import com.eveningbread.fileservice.core.exception.FileServiceException;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author tom
 */
@Data
@AllArgsConstructor
public class BaseResponse {

    private String message;
    private boolean result;
    private int code;

    public BaseResponse() {
        this.message = "";
        this.result = false;
        this.code = ResponseCode.UNSPECIFIED_CODE;
    }

    public BaseResponse(String message, int code) {
        this(message, false, code);
    }

    protected BaseResponse(BaseResponse baseResponse) {
        this(baseResponse.getMessage(), baseResponse.isResult(), baseResponse.getCode());
    }

    public static class Builder {

        private BaseResponse baseResponse = null;

        public Builder of() {
            this.baseResponse = new BaseResponse();
            return this;
        }

        public Builder success() {
            this.baseResponse.result = true;
            this.baseResponse.code = ResponseCode.SUCCESS;
            return this;
        }

        public Builder and() {
            if (null == baseResponse) {
                throw new FileServiceException("Failure to build BaseResponse");
            }
            return this;
        }

        public Builder message(String message) {
            this.baseResponse.message = message;
            return this;
        }

        public Builder result(boolean result) {
            this.baseResponse.result = result;
            return this;
        }

        public Builder resultFalse() {
            this.baseResponse.result = Boolean.FALSE;
            return this;
        }

        public Builder resultTrue() {
            this.baseResponse.result = Boolean.TRUE;
            return this;
        }

        public Builder code(int code) {
            this.baseResponse.code = code;
            return this;
        }

        public BaseResponse build() {
            if (null == baseResponse) {
                throw new FileServiceException("Failure to build BaseResponse");
            }
            return this.baseResponse;
        }
    }
}
