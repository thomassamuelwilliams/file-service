package com.eveningbread.fileservice.core.exception;

import com.eveningbread.fileservice.core.constants.ResponseCode;
import lombok.Getter;

/**
 * @author tom
 */
@Getter
public class FileServiceException extends RuntimeException {

    private static final String DEFAULT_MESSAGE = "An error occurred.";
    private static final int DEFAULT_CODE = ResponseCode.INTERNAL_SERVER_ERROR;

    private final int code;

    public FileServiceException(String message) {
        this(message, null);
    }

    public FileServiceException(String message, Throwable cause) {
        this(message, cause, DEFAULT_CODE);
    }

    public FileServiceException(String message, Throwable cause, int code) {
        super(message, cause);
        this.code = code;
    }
}
