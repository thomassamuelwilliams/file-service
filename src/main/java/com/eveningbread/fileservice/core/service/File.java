package com.eveningbread.fileservice.core.service;

import lombok.Data;

/**
 * @author tom
 */
@Data
public class File {

    private String originalFileName;
    private String targetPath;

}
