package com.eveningbread.fileservice.core.service;

import com.eveningbread.fileservice.core.exception.FileServiceException;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author tom
 */
@Service
public class FilePathService {

    private static final Logger logger = LogManager.getLogger();
    private static final String fileSeparator = "/";
    private static final int FILE_NAME_PRESERVATION_INDEX = 10;

    @Value("${date.time.format}")
    private String dateTimeFormat;

    public String prepareFilePath(String originalFileName, String relativePath) {
        validate(originalFileName, relativePath);

        final String savedFileName = nameFile(originalFileName);

        if (!StringUtils.hasLength(relativePath)) {
            logger.warn("Not adding a relative path for file {}.", originalFileName);
            return savedFileName;
        }

        return relativePath + fileSeparator + savedFileName;
    }

    public String nameFile(String originalFileName) {
        final String extension = FilenameUtils.getExtension(originalFileName);

        final String rawFileName = FilenameUtils.removeExtension(originalFileName);
        final String savedName = rawFileName.length() > FILE_NAME_PRESERVATION_INDEX
                ? rawFileName.substring(0, FILE_NAME_PRESERVATION_INDEX)
                : rawFileName;

        return savedName
                + "-"
                + LocalDateTime.now().format(DateTimeFormatter.ofPattern(dateTimeFormat))
                + "."
                + extension;
    }

    private void validate(String originalFileName, String relativePath) {
        if (!StringUtils.hasLength(originalFileName)) {
            logger.error("File for relativePath {} has no original file name", relativePath);
            throw new FileServiceException("File for relativePath " + relativePath + " has no file name");
        }

        final String extension = FilenameUtils.getExtension(originalFileName);
        if (StringUtils.hasLength(extension)) {
            final String rawFileName = FilenameUtils.removeExtension(originalFileName);
            if (!StringUtils.hasLength(rawFileName)) {
                throw new FileServiceException("original file name: " + originalFileName + " is just an extension");
            }
        }

        if (!StringUtils.hasLength(relativePath)) {
            return;
        }

        // relative path validation
        if (relativePath.startsWith(fileSeparator)) {
            throw new FileServiceException("File with original file name: " + originalFileName +
                    " starts with file seperator: " + fileSeparator);
        }
        if (relativePath.endsWith(fileSeparator)) {
            throw new FileServiceException("File with original file name: " + originalFileName +
                    " ends with file seperator: " + fileSeparator);
        }
    }
}
