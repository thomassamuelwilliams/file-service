package com.eveningbread.fileservice.core.service;

import com.eveningbread.fileservice.core.exception.FileServiceException;
import com.eveningbread.fileservice.core.properties.StorageProperties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/**
 * @author tom
 */
@Service
public class FileService {

    private static final Logger logger = LogManager.getLogger();

    private final FilePathService filePathService;

    private final Path rootLocation;

    public FileService(FilePathService filePathService, StorageProperties properties) {
        this.filePathService = filePathService;
        this.rootLocation = Paths.get(properties.getLocation());
    }

    public File copyFile(MultipartFile multipartFile, String relativePath) {
        final File file = new File();

        final String originalFilename = multipartFile.getOriginalFilename();
        file.setOriginalFileName(originalFilename);

        if (multipartFile.isEmpty()) {
            logger.error("File {} is empty", originalFilename);
            throw new FileServiceException("Empty file.");
        }

        final String savedTargetPath = filePathService.prepareFilePath(originalFilename, relativePath);
        file.setTargetPath(savedTargetPath);

        Path destinationFile = this.rootLocation.resolve(savedTargetPath).normalize().toAbsolutePath();
        logger.debug("Creating directories {} if needed", destinationFile.getParent().toString());
        try {
            Files.createDirectories(destinationFile.getParent());
        } catch (IOException e) {
            logger.error("Error creating directories", e);
            throw new FileServiceException("Error creating directories", e);
        }

        logger.debug("Copying files to destination: {}", destinationFile);
        // the number of bytes read or written
        final long bytesWritten;
        try (InputStream inputStream = multipartFile.getInputStream()) {
            bytesWritten = Files.copy(inputStream, destinationFile, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            logger.error("Error copying file", e);
            throw new FileServiceException("Error copying file", e);
        }

        if (1 > bytesWritten) {
            throw new FileServiceException("No bytes written");
        }
        logger.info("{} bytes written", bytesWritten);

        return file;
    }

}
