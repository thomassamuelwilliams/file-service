package com.eveningbread.fileservice.upload.controller;

import com.eveningbread.fileservice.core.constants.ResponseCode;
import com.eveningbread.fileservice.core.exception.FileServiceException;
import com.eveningbread.fileservice.upload.dto.UploadFileResponse;
import com.eveningbread.fileservice.upload.dto.UploadFilesResponse;
import com.eveningbread.fileservice.upload.service.UploadService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author tom
 */
@RestController
@RequestMapping("/upload")
public class UploadController {

    private static final Logger logger = LogManager.getLogger(UploadController.class);

    private final UploadService uploadService;

    public UploadController(UploadService uploadService) {
        this.uploadService = uploadService;
    }

    @PostMapping("/file")
    public UploadFileResponse uploadFile(
            @RequestParam MultipartFile file, @RequestParam(required = false) String relativePath) {
        try {
            return uploadService.uploadFile(file, relativePath);
        } catch (FileServiceException e) {
            return new UploadFileResponse(e.getMessage(), e.getCode());
        } catch (Exception e) {
            return new UploadFileResponse(e.getMessage(), ResponseCode.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/files")
    public UploadFilesResponse uploadFiles(
            @RequestParam List<MultipartFile> files, @RequestParam(required = false) String relativePath) {
        try {
            return uploadService.uploadFiles(files, relativePath);
        } catch (FileServiceException e) {
            return new UploadFilesResponse(e.getMessage(), e.getCode());
        } catch (Exception e) {
            return new UploadFilesResponse(e.getMessage(), ResponseCode.INTERNAL_SERVER_ERROR);
        }
    }
}
