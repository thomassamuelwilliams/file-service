package com.eveningbread.fileservice.upload.dto;

import com.eveningbread.fileservice.core.dto.BaseResponse;
import com.eveningbread.fileservice.core.service.File;
import lombok.Getter;
import lombok.Setter;

/**
 * @author tom
 */
@Getter
@Setter
public class UploadFileResponse extends BaseResponse {

    private File file;

    public UploadFileResponse(String message, int code) {
        super(message, code);
    }

    public UploadFileResponse(BaseResponse baseResponse, File file) {
        super(baseResponse);
        this.file = file;
    }
}
