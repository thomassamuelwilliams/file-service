package com.eveningbread.fileservice.upload.dto;

import com.eveningbread.fileservice.core.dto.BaseResponse;
import com.eveningbread.fileservice.core.service.File;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author tom
 */
@Getter
@Setter
public class UploadFilesResponse extends BaseResponse {

    List<File> files;

    public UploadFilesResponse(String message, int code) {
        super(message, code);
    }

    public UploadFilesResponse(BaseResponse baseResponse, List<File> files) {
        super(baseResponse);
        this.files = files;
    }

}
