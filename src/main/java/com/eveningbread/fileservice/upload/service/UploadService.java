package com.eveningbread.fileservice.upload.service;

import com.eveningbread.fileservice.core.dto.BaseResponse;
import com.eveningbread.fileservice.core.service.File;
import com.eveningbread.fileservice.core.service.FileService;
import com.eveningbread.fileservice.upload.dto.UploadFileResponse;
import com.eveningbread.fileservice.upload.dto.UploadFilesResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

/**
 * @author tom
 */
@Service
public class UploadService {

    private final FileService fileService;

    public UploadService(FileService fileService) {
        this.fileService = fileService;
    }

    public UploadFileResponse uploadFile(MultipartFile multipartFile, String relativePath) {
        final File file = fileService.copyFile(multipartFile, relativePath);

        return new UploadFileResponse(
                new BaseResponse.Builder().of().success().build(),
                file);
    }

    public UploadFilesResponse uploadFiles(List<MultipartFile> multipartFiles, String relativePath) {
        List<File> files = new ArrayList<>();

        for (MultipartFile multipartFile : multipartFiles) {
            final File file = fileService.copyFile(multipartFile, relativePath);
            files.add(file);
        }

        return new UploadFilesResponse(
                new BaseResponse.Builder().of().success().build(),
                files);
    }
}
